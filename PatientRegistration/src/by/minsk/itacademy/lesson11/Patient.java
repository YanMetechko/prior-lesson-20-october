package by.minsk.itacademy.lesson11;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Objects;

public class Patient {
    private String name;
    private String surname;
    private GregorianCalendar birthday;
    private boolean isIll;

    public Patient(String name, String surname, int year, int month, int dayOfMonth, boolean isIll) {
        this.name = name;
        this.surname = surname;
        this.birthday = new GregorianCalendar(year, month, dayOfMonth);
        this.isIll = isIll;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatter=new SimpleDateFormat("dd.MM.yyyy");
        String date = formatter.format(birthday.getTime());
        return  (name + ';' + surname + ';' + date);
    }

    public Patient(String name, String surname, GregorianCalendar birthDay, boolean isIll) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthDay;
        this.isIll = isIll;


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(name, patient.name) && Objects.equals(surname, patient.surname) && Objects.equals(birthday, patient.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthday);
    }

    public boolean isHeIll() {
        return isIll;
    }
}
