package by.minsk.itacademy.lesson11;

import java.util.*;

public class Registr {
    private Map<Patient, Boolean> patientList = new HashMap<>();

   /* public Registr(Map<Patient, Boolean> patientList) {
        this.patientList = patientList;
    }*/


    public void addAPatient() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Insert a name, surname, year of Birth, month of Birth, Day of Birth and if a person is ill:");
        String name = scan.next();
        String surname = scan.next();
        int year = scan.nextInt();
        int month = scan.nextInt();
        int day = scan.nextInt();
        boolean isIll = scan.nextBoolean();
        Patient patient = new Patient(name, surname, year, month-1, day, isIll);
        patientList.put(patient, patient.isHeIll());
    }

    public void addFromFile(List<Patient> list) {
        ListIterator<Patient> listIter = list.listIterator();
        while (listIter.hasNext()) {
            Patient patient = listIter.next();
            patientList.put(patient, patient.isHeIll());
        }
    }

    public void printOut() {
        Set<Patient> set = patientList.keySet();
        Iterator<Patient> itr = set.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
    }

    public Map<Patient, Boolean> getPatientList() {
        return patientList;
    }
}
