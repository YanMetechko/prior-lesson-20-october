package by.minsk.itacademy.lesson11;

import java.io.File;

public class Main {

    public static void main(String[] args) {
        File textfile = new File("patients.txt");
        File binfile = new File("trial.dat");
        Registr registr = new Registr();


       FileHandler operator = new FileHandler();
       registr.addFromFile(operator.textFileRead(textfile));
        registr.addAPatient();
        operator.fileWrite(registr.getPatientList(),binfile);
       registr.addFromFile(operator.binFileRead(binfile));
       registr.printOut();
    }
}
