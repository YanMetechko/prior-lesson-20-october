package by.minsk.itacademy.lesson11;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Boolean.getBoolean;

public class FileHandler {

    public List<Patient> textFileRead(File file) {
        List<Patient> outOfFileList = new ArrayList<>();
        try (BufferedReader viewer = new BufferedReader(new FileReader(file))) {
            String temp = viewer.readLine();
            while (temp != null) {
                String[] buffer = temp.split(";");
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Date date = format.parse(buffer[2]);
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                outOfFileList.add(new Patient(buffer[0], buffer[1], calendar, getBoolean(buffer[3])));
                temp = viewer.readLine();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return outOfFileList;
    }

    public List<Patient> binFileRead(File file) {
        List<Patient> outOfFileList = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            while (ois.read() != -1) {
                String[] buffer = ois.readUTF().split(";");
                boolean illness = ois.readBoolean();
                SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
                Date date = format.parse(buffer[2]);
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(date);
                outOfFileList.add(new Patient(buffer[0], buffer[1], calendar, illness));
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return outOfFileList;
    }

    public void fileWrite(Map<Patient, Boolean> map, File file) {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file, false))) {
            Set<Patient> set = map.keySet();
            Iterator<Patient> itr = set.iterator();
            while (itr.hasNext()) {
                Patient patient = itr.next();
                dos.writeUTF(patient.toString());
                dos.writeBoolean(map.get(patient));
            }
        } catch (Exception f) {
            System.out.println(f.getMessage());
        }
    }

}




