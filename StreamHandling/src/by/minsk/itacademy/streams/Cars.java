package by.minsk.itacademy.streams;

import java.io.Serializable;

public class Cars implements Serializable {
    private boolean isProduced;
    private long quantityProduced;
    private String model;

    public Cars(String model, boolean isProduced, long quantityProduced) {
        this.model = model;
        this.isProduced = isProduced;
        this.quantityProduced = quantityProduced;
    }

    @Override
    public String toString() {
        return "Cars{" + "isProduced=" + isProduced + ", quantityProduced=" + quantityProduced + ", model='" + model + '\'' + '}';
    }
}
