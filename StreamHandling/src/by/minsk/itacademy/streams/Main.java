package by.minsk.itacademy.streams;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

List<Cars> minskCatalog = new ArrayList<>();
minskCatalog.add(new Cars("AZLK", false, 2345067));
minskCatalog.add(new Cars("Volga",true, 751453884));
minskCatalog.add(new Cars("lada",true, 952456987));



try (ObjectOutputStream ops = new ObjectOutputStream(new FileOutputStream("catalog.dat"))){
    ops.writeObject(minskCatalog);
}
catch(IOException ex) {
    System.out.println(ex.getMessage());
        }

ArrayList<Cars> sameCatalog = new ArrayList<>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("catalog.dat"))){
            sameCatalog = (ArrayList<Cars>)ois.readObject();
            for (Cars car:sameCatalog) {
                System.out.println(car);
            }
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
